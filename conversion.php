<html>
	<head>
		<meta charset="utf-8">
		<title>Uso de Metodo Post</title>
		<link rel="stylesheet" type="text/css" href="estilos/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="estilos/css/bootstrap.min.css">
		<style type="text/css">
		label{
			background-color: red;
			color: white;
			font-size: 18px;
		}
		</style>
	</head>
	<body>
		<div class="conteiner">
		<h2>Coversion Decimal a Binario</h2>
		<form class="form-inline" name="frmPost" method="POST">
			Escriba el Decimal:
			<input class="form-control" type="text" name="txtNumero">
			<input class="btn btn-primary" type="submit" name="btnEnviar" value="Convertir">
		</form>
		<?php
			if (!empty($_POST['txtNumero'])) {
				$numeroDecimal = $_POST['txtNumero'];
				echo "El numero decimal es: ".$numeroDecimal. "<br>";

				$numBinario =' ';
				echo "<br>";
				echo "<h2>Procedimiento: </h2>";
				echo "<table border=1 bgcolor=white>
							<thead>
								<th>Dividendo</th>
								<th>Divisor</th>
								<th>Cociente</th>
								<th>Residuo</th>
							</thead>";
				do {

					$dividendo = $numeroDecimal;
					echo "<tr>";
					echo "<td>$dividendo</td>";

					$divisor = 2;
					echo "<td>$divisor</td>";

					$cociente = (int)($numeroDecimal/2);
					echo "<td>$cociente</td>";

					$residuo = $numeroDecimal % 2;
					echo "<td style=color:red>$residuo</td>";

					
					$numBinario = $numeroDecimal % 2 . $numBinario;
					$numeroDecimal = (int)($numeroDecimal/2);

				} while ($numeroDecimal > 0);
				echo "</tr>";
				echo "</table>";
				echo "<h2>Numero Binario: $numBinario</h2>";

			}else{
				echo "<label> Debe de ingresar un numero para realizar la conversion </label>";
			}
		?>
		</div>
	</body>
</html>