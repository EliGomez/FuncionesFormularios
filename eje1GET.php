<html>
	<head>
		<title>Uso de Metodo GET</title>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="estilos/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="estilos/css/bootstrap.min.css">
	</head>
	<body>
		<div class="container">
			<h2>Coversion Decimal a Binario</h2>
			<form class="form-inline" name="frmPost" method="GET">
				Escriba el Decimal:
				<input type="text" class="form-control" name="txtNumero">
				<input type="submit" class="btn btn-primary" name="btnEnviar">
			</form>
			<?php
				if (!empty($_GET['txtNumero'])) {
					$numeroDecimal = $_GET['txtNumero'];
					echo "Elnumero decimal es: ".$numeroDecimal. "<br>";

					$numBinario =' ';
					do {
						$numBinario = $numeroDecimal % 2 . $numBinario;
						$numeroDecimal = (int)($numeroDecimal/2);
					} while ($numeroDecimal > 0);
					echo "Numero Binario:".$numBinario;
				}
			?>
		</div>
	</body>
</html>