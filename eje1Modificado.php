<html>
	<head>
		<meta charset="utf-8">
		<title>Uso de Metodo Post</title>
		<link rel="stylesheet" type="text/css" href="estilos/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="estilos/css/bootstrap.min.css">
		<style type="text/css">
		label{
			background-color: red;
			color: white;
			font-size: 18px;
		}
		</style>
	</head>
	<body>
		<div class="container">
			<h2>Coversion Decimal a Binario</h2>
			<form class="form-inline" name="frmPost" method="POST">
				Escriba el Decimal:
				<input type="text" class="form-control" name="txtNumero">
				<input type="submit" name="btnEnviar" class="btn btn-primary" value="Convertir">
			</form>
			<?php
				if (!empty($_POST['txtNumero'])) {
					$numeroDecimal = $_POST['txtNumero'];
					echo "El numero decimal es: ".$numeroDecimal. "<br>";

					$numBinario =' ';
					do {
						$numBinario = $numeroDecimal % 2 . $numBinario;
						$numeroDecimal = (int)($numeroDecimal/2);
					} while ($numeroDecimal > 0);
					echo "Numero Binario:".$numBinario;
				}else{
					echo "<label> Debe de ingresar un numero para realizar la conversion </label>";
				}
			?>
		</div>
	</body>
</html>