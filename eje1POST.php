<html>
	<head>
		<title>Uso de Metodo Post</title>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="estilos/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="estilos/css/bootstrap.min.css">
	</head>
	<body>
		<div class="container">
			<h2>Coversion Decimal a Binario</h2>
			<form class="form-inline" name="frmPost" method="POST">
				Escriba el Decimal:
				<input type="text" class="form-control" name="txtNumero" size="50">
				<input type="submit" class="btn btn-primary" style="bgcolor:green" name="btnEnviar">
			</form>
			<?php
				if (!empty($_POST['txtNumero'])) {
					$numeroDecimal = $_POST['txtNumero'];
					echo "El numero decimal es: ".$numeroDecimal. "<br>";

					$numBinario =' ';
					do {
						$numBinario = $numeroDecimal % 2 . $numBinario;
						$numeroDecimal = (int)($numeroDecimal/2);
					} while ($numeroDecimal > 0);
					echo "Numero Binario:".$numBinario;
				}
			?>
		</div>
	</body>
</html>