<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Ejercicio 2</title>
		</head>	
	<body>
		<div align="center">
			<h2>Notas de Estudiantes</h2>
			<?php 

				$notas = array(
					array('nombre' =>'Alicia', 'nota1'=>8,'nota2'=>8, 'nota3'=>10),
					array('nombre' =>'Monica', 'nota1'=>7,'nota2'=>8, 'nota3'=>9),
					array('nombre' =>'Manuel', 'nota1'=>5,'nota2'=>8, 'nota3'=>10),
					array('nombre' =>'Alejandro', 'nota1'=>9,'nota2'=>7, 'nota3'=>7),
					array('nombre' =>'Ana', 'nota1'=>7,'nota2'=>8, 'nota3'=>9),
					array('nombre' =>'Lourdes', 'nota1'=>8,'nota2'=>7, 'nota3'=>9),
					array('nombre' =>'Adolfo', 'nota1'=>9,'nota2'=>9, 'nota3'=>9),
					array('nombre' =>'Edenilson', 'nota1'=>6,'nota2'=>6, 'nota3'=>6),
					array('nombre' =>'Lucia', 'nota1'=>8,'nota2'=>7, 'nota3'=>9),
					array('nombre' =>'Angelica', 'nota1'=>10,'nota2'=>9, 'nota3'=>10)
				);

				echo "<table border=1>
				<thead>
					<th>Nombre</th>
					<th>Nota 1</th>
					<th>Nota 2</th>
					<th>Nota 3</th>
					<th>Promedio</th>
				</thead>
				<tbody>
				<tr>";

				for ($i=0; $i <count($notas); $i++)
				{ 
					foreach ($notas[$i] as $valor) 
					{
						echo "<td>". $valor ."</td>";
					}

					$suma=$notas[$i]['nota1']+$notas[$i]['nota2']+$notas[$i]['nota3'];
					$promedio=$suma/3;

					echo "<td>".round($promedio,PHP_ROUND_HALF_UP)."</td>";
					echo "</tr>";
					
				}
			?>
		</div>
	</body>
</html>